import "./Oval.scss";

const Oval = ({ color }) => {
  return <div className={`oval oval_${color}`}> </div>;
};
export default Oval;
