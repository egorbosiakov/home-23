import Button from "../Button/Button";
import "./Header.scss";

const Header = () => {
  return (
    <header className="header">
      <div className="header__inner">
        <h1 className="header__text">Текст</h1>

        <Button text="Кнопка 1" />
      </div>
    </header>
  );
};

export default Header;
