import Button from "../Button/Button";
import "./Footer.scss";

const Footer = () => {
  return (
    <footer className="footer">
      <Button mod="button_foot" text="Кнопка 2" />
    </footer>
  );
};

export default Footer;
