import { Component } from "react";
import "./Button.scss";

class Button extends Component {
  click = () => {
    console.log(this.props.text);
  };

  render() {
    const { mod, text } = this.props;

    return (
      <button onClick={this.click} className={`button ${mod || ""}`}>
        {text}
      </button>
    );
  }
}
export default Button;
