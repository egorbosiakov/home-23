import "./Main.scss";
import Oval from "../Oval/Oval.js";

const Main = () => {
  const arr = [
    { color: "gray" },
    { color: "yellow" },
    { color: "black" },
    { color: "yellow" },
    { color: "yellow" },
    { color: "orange" },
    { color: "orange" },
    { color: "yellow" },
  ];

  return (
    <main className="main">
      {arr.map((ithem, index) => (
        <Oval color={ithem.color} />
      ))}
    </main>
  );
};
export default Main;
