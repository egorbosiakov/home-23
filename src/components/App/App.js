import { Fragment } from "react";
import "./App.scss";
import Footer from "../Footer/Footer.js";
import Header from "../Header/Header.js";
import Main from "../Main/Main.js";

const App = () => {
  return (
    <Fragment>
      <Header />
      <Main />
      <Footer />
    </Fragment>
  );
};

export default App;
